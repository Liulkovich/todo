//
//  Router.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import UIKit

protocol Router {
    func open(with viewController: UIViewController)
    func close()
}
