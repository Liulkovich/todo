//
//  NavigationRouter.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import UIKit

class NavigationRouter: Router {
    
    private var navigationController: UINavigationController?
       
    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
       
    func open(with viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: false)
    }
       
    func close() {
        navigationController?.popViewController(animated: false)
    }
}
