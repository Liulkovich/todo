//
//  TasksAssembly.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import UIKit

final class TasksAssembly {
    
    func tasksController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Tasks", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TasksViewController")
        (viewController as? TasksViewController)?.viewModel = viewModel()
        return viewController
    }
}

private extension TasksAssembly {

    func viewModel() -> TasksViewModelImp {
        let serverURL = URL(string: "realms://todorl.us1a.cloud.realm.io/~/ToDo")!
        let taskManager = TaskManagerImp(realmURL: serverURL)
        let viewModel = TasksViewModelImp(taskManager: taskManager)
        return viewModel
    }
}
