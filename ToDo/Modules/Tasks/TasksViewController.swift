//
//  TasksViewController.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import UIKit

class TasksViewController: UITableViewController {
    
    var viewModel: TasksViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTaskAlert))
        navigationItem.hidesBackButton = true
    }
}

private extension TasksViewController {
    
    @objc func addTaskAlert() {
        
        let alertController = UIAlertController(title: "Add Item", message: "", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "Save", style: .default, handler: { [weak self] alert -> Void in
            guard let body = alertController.textFields?[0].text else { return }

            self?.viewModel?.addTask(with: body, completion: {
                self?.tableView.beginUpdates()
                self?.tableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
                self?.tableView.endUpdates()
            })
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = "New Item Text"
        })
        
        present(alertController, animated: true, completion: nil)
    }
}

extension TasksViewController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.tasksCount ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") ?? UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.selectionStyle = .none
        let task = viewModel?.task(with: indexPath.row)
        cell.textLabel?.text = task?.body
        cell.accessoryType = (task?.isDone ?? false) ? UITableViewCell.AccessoryType.checkmark : UITableViewCell.AccessoryType.none
        return cell
    }
}

extension TasksViewController {
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.changeStatusTask(with: indexPath.row, completion: {
            tableView.beginUpdates()
            tableView.reloadRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        })
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        viewModel?.removeTask(with: indexPath.row, completion: {
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        })
    }
}
