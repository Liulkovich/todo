//
//  TasksViewModel.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import Foundation

protocol TasksViewModel {

    var tasksCount: Int { get }

    func task(with id: Int) -> Task
    func addTask(with body: String, completion: @escaping () -> Void)
    func changeStatusTask(with id: Int, completion: @escaping () -> Void)
    func removeTask(with id: Int, completion: @escaping () -> Void)
}

final class TasksViewModelImp {
    
    private let taskManager: TaskManager
    
    init(taskManager: TaskManager) {
        self.taskManager = taskManager
    }
}

extension TasksViewModelImp: TasksViewModel {
    
    var tasksCount: Int {
        return taskManager.tasks.count
    }
    
    func task(with id: Int) -> Task {
        return taskManager.tasks[id]
    }
    
    func addTask(with body: String, completion: @escaping () -> Void) {
        taskManager.addTask(with: body) { added in
            guard added else {
                return
            }
            completion()
        }
    }
    
    func changeStatusTask(with id: Int, completion: @escaping () -> Void) {
        taskManager.changeStatusTask(with: id) { changed in
            guard changed else {
                return
            }
            completion()
        }
    }
    
    func removeTask(with id: Int, completion: @escaping () -> Void) {
        taskManager.removeTask(with: id) { removed in
            guard removed else {
                return
            }
            completion()
        }
    }
}
