//
//  AuthenticationAssembly.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import UIKit

final class AuthenticationAssembly {
    
    func authenticationController() -> UIViewController {
        let storyboard = UIStoryboard(name: "Authentication", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "AuthenticationViewController")
        let navigationController = UINavigationController(rootViewController: viewController)
        
        (viewController as? AuthenticationViewController)?.viewModel = viewModel(router: router(baseController: navigationController))
        
        return navigationController
    }
}

private extension AuthenticationAssembly {

    func viewModel(router: AuthenticationRouter) -> AuthenticationViewModelImp {
        let serverURL = URL(string: "https://todorl.us1a.cloud.realm.io")!
        let authenticationManager = AuthenticationManagerImp(serverURL: serverURL)
        let viewModel = AuthenticationViewModelImp(authenticationManager: authenticationManager, router: router)
        return viewModel
    }
    
    func router(baseController: UINavigationController) -> AuthenticationRouter {
        return AuthenticationRouter(navigationController: baseController)
    }
}
