//
//  AuthenticationViewModel.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import Foundation

protocol AuthenticationViewModel {
    
    var errorCallback: ((String) -> Void)? { get set }
    
    func checkAuthentication()
    func signIn(username: String?, password: String?)
    func signUp(username: String?, password: String?)
}

final class AuthenticationViewModelImp {
    
    var errorCallback: ((String) -> Void)?
    
    private let authenticationManager: AuthenticationManager
    private let router: AuthenticationRouter
    
    init(authenticationManager: AuthenticationManager, router: AuthenticationRouter) {
        self.authenticationManager = authenticationManager
        self.router = router
    }
}

extension AuthenticationViewModelImp: AuthenticationViewModel {
    
    func checkAuthentication() {
        guard authenticationManager.authenticationStatus == .authorized else {
            return
        }
        
        router.openTasksScreen()
    }
    
    func signIn(username: String?, password: String?) {
        guard let username = username, let password = password, !username.isEmpty, !password.isEmpty else {
            errorCallback?("Empty data")
            return
        }
        
        authenticationManager.signIn(username: username, password: password) { [weak self] status, error in
            switch status {
            case .authorized:
                self?.router.openTasksScreen()
            case .notAuthorized:
                self?.errorCallback?(error?.localizedDescription ?? "Unable to authenticate")
            }
        }
    }
    
    func signUp(username: String?, password: String?) {
        guard let username = username, let password = password, !username.isEmpty, !password.isEmpty else {
            errorCallback?("Empty data")
            return
        }
        
        authenticationManager.signUp(username: username, password: password) { [weak self] status, error in
            switch status {
            case .authorized:
                self?.router.openTasksScreen()
            case .notAuthorized:
                self?.errorCallback?(error?.localizedDescription ?? "Unable to authenticate")
            }
        }
    }
}
