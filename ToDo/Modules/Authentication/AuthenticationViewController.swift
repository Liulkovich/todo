//
//  AuthenticationViewController.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import UIKit

class AuthenticationViewController: UIViewController {
    
    @IBOutlet private weak var loginTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    
    var viewModel: AuthenticationViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
    }
}

private extension AuthenticationViewController {
    
    @IBAction func signIn(_ sender: Any) {
        viewModel?.signIn(username: loginTextField.text, password: passwordTextField.text)
    }
    
    @IBAction func signUp(_ sender: Any) {
        viewModel?.signUp(username: loginTextField.text, password: passwordTextField.text)
    }
    
    func bindViewModel() {
        guard isViewLoaded else { return }
        
        viewModel?.checkAuthentication()
        
        viewModel?.errorCallback = { error in
            print(error)
        }
    }
}
