//
//  AuthenticationRouter.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import Foundation

final class AuthenticationRouter: NavigationRouter {
    
    func openTasksScreen() {
        let viewController = TasksAssembly().tasksController()
        open(with: viewController)
    }
}
