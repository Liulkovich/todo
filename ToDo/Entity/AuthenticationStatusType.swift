//
//  AuthenticationStatusType.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/15/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import Foundation

enum AuthenticationStatusType {

    case authorized
    case notAuthorized
}
