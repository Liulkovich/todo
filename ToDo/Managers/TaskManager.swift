//
//  TaskManager.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import Foundation

protocol TaskManager {
    
    var tasks: [Task] { get }
    
    func addTask(with body: String, completion: @escaping (_ added: Bool) -> Void)
    func changeStatusTask(with id: Int, completion: @escaping (_ changed: Bool) -> Void)
    func removeTask(with id: Int, completion: @escaping (_ removed: Bool) -> Void)
}
