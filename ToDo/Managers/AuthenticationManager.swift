//
//  AuthenticationManager.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/15/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import Foundation

protocol AuthenticationManager {
    
    typealias AuthenticationResult = (_ status: AuthenticationStatusType, _ error: Error?) -> Void
    
    var authenticationStatus: AuthenticationStatusType { get }
    
    func signIn(username: String, password: String, completion: @escaping AuthenticationResult)
    func signUp(username: String, password: String, completion: @escaping AuthenticationResult)
}
