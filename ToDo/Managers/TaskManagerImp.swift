//
//  TaskManagerImp.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/16/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import Foundation
import RealmSwift

final class TaskManagerImp {
    
    private let realm: Realm
    private let items: Results<Task>
    
    init(realmURL: URL) {
        guard let config = SyncUser.current?.configuration(realmURL: realmURL, fullSynchronization: true), let realm = try? Realm(configuration: config) else {
            fatalError()
        }
        self.realm = realm
        self.items = realm.objects(Task.self).sorted(byKeyPath: "timestamp", ascending: false)
    }
}

extension TaskManagerImp: TaskManager {
    
    var tasks: [Task] {
        return items.map { $0 }
    }
    
    func addTask(with body: String, completion: @escaping (_ added: Bool) -> Void) {
        let item = Task()
        item.body = body
    
        do {
            try realm.write {
                realm.add(item)
                completion(true)
            }
        } catch {
            completion(false)
        }
    }
    
    func changeStatusTask(with id: Int, completion: @escaping (_ changed: Bool) -> Void) {
        let item = items[id]

        do {
            try realm.write {
                item.isDone = !item.isDone
                completion(true)
            }
        } catch {
            completion(false)
        }
    }
    
    func removeTask(with id: Int, completion: @escaping (_ removed: Bool) -> Void) {
        let item = items[id]

        do {
            try realm.write {
                realm.delete(item)
                completion(true)
            }
        } catch {
            completion(false)
        }
    }
}
