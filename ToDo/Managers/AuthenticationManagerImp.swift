//
//  AuthenticationManagerImp.swift
//  ToDo
//
//  Created by Raman Liulkovich on 6/15/20.
//  Copyright © 2020 Raman Liulkovich. All rights reserved.
//

import Foundation
import RealmSwift

final class AuthenticationManagerImp {
    
    private let serverURL: URL
    
    init(serverURL: URL) {
        self.serverURL = serverURL
    }
}

extension AuthenticationManagerImp: AuthenticationManager {
    
    var authenticationStatus: AuthenticationStatusType {
        guard SyncUser.all.count != 1 else {
            return .authorized
        }
        
        SyncUser.all.forEach { _, user in
            user.logOut()
        }
        
        return .notAuthorized
    }
    
    func signIn(username: String, password: String, completion: @escaping AuthenticationResult) {
        let credentials = SyncCredentials.usernamePassword(username: username, password: password, register: false)
        sendCredentials(credentials: credentials, completion: completion)
    }
    
    func signUp(username: String, password: String, completion: @escaping AuthenticationResult) {
        let credentials = SyncCredentials.usernamePassword(username: username, password: password, register: true)
        sendCredentials(credentials: credentials, completion: completion)
    }
}

private extension AuthenticationManagerImp {
    
    func sendCredentials(credentials: SyncCredentials, completion: @escaping AuthenticationResult) {
        SyncUser.logIn(with: credentials, server: serverURL) { user, error in
            if let error = error {
                completion(.notAuthorized, error)
            } else if user == nil {
                completion(.notAuthorized, nil)
            } else {
                completion(.authorized, nil)
            }
        }
    }
}
